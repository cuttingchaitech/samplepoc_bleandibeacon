//
//  ViewController.swift
//  SampleiBeaconAndBLEPOC
//
//  Created by Salman Qureshi on 7/21/20.
//  Copyright © 2020 Salman Qureshi. All rights reserved.
//

import UIKit
import Foundation
import CoreBluetooth
import CoreLocation

//In this class scanning through both Core location(iBaacon ranging) and Core bluethooth and advertsing only through Core BLE

class ViewController: UIViewController {

    //Core Bluettoh GATT service property
    private var peripheralManager = CBPeripheralManager()
    private var centralManager = CBCentralManager()
    private var locationManager = CLLocationManager()
    
    private var serviceUUID: [CBUUID] = []
    
    private var txService = CBUUID()
    
    private var discoveredProfileIds:[String] = []
    
    //iBeacon property
    private var txServiceUUID = [UUID]()
    var beaconRegion: CLBeaconRegion?
    fileprivate var beacons: [CLBeacon] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //txService for advertising
        txService = CBUUID(string: Constant.advertisingID)
        
        //Get all UUID to scan for
        Constant.allUsersToScanFor.forEach { (userId) in
            let eachStoreUUID = CBUUID(string: userId)
            serviceUUID.append(eachStoreUUID)
            
            if let eachUUID = UUID(uuidString: userId) {
                txServiceUUID.append(eachUUID)
            }
        }
        
        
        startScanning()
        startRangingBeacon()
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
    }
    
    private func requestLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.startUpdatingLocation()
    }
    
    func startRangingBeacon() {
        
        txServiceUUID.enumerated().forEach { (arg0) in
            let (_, _) = arg0
            
            beaconRegion = CLBeaconRegion(proximityUUID: arg0.element,major: 1, minor: 1, identifier: "\(arg0.element)")
            
            locationManager.startRangingBeacons(in: beaconRegion!)
        }
    }
    
    private func stopRanging() {
        
        locationManager.rangedRegions.forEach { (region) in
            self.locationManager.stopRangingBeacons(in: region as! CLBeaconRegion)
        }
    }

    private func startScanning() {
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
    }
    
    private func startAdvertising() {
        
        if (peripheralManager.isAdvertising) {
            peripheralManager.stopAdvertising()
        }
        
        let service = CBMutableService(type: txService, primary: true)
        peripheralManager.add(service)
        
        let advertisingData: [String : Any] = [
            CBAdvertisementDataServiceUUIDsKey: [service.uuid]
        ]
        
        peripheralManager.startAdvertising(advertisingData)
    }
    
    public func stopAdvertising() {
        peripheralManager.stopAdvertising()
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.unknown }
        
        if knownBeacons.count > 0 {
            let nearestBeacon = knownBeacons.first!
            let major = CLBeaconMajorValue(truncating: nearestBeacon.major)
            let minor = CLBeaconMinorValue(truncating: nearestBeacon.minor)
            
            let accuracy = String(format: "%.2f", nearestBeacon.accuracy)
            
            print("Proximity: \(nameForProximity(nearestBeacon.proximity)) Accuracy: approx. \(accuracy)m, RSSI: \(nearestBeacon.rssi), UUID: \(nearestBeacon.uuid), major: \(major), minor: \(minor)")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed: \(error.localizedDescription)")
    }
    
    func nameForProximity(_ proximity: CLProximity) -> String {
        switch proximity {
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .near:
            return "Near"
        case .far:
            return "Far"
        @unknown default:
            fatalError()
        }
    }
}

extension ViewController: CBCentralManagerDelegate {
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        //            if let _peripheralName = peripheral.name {
        //                print("PERIPHERAL NAME: \(_peripheralName)")
        //            }
        print("----------------------Central Discover----------------------")
        
        print("ADVERTISEMENT DATA -> \(advertisementData), RSSI -> \(RSSI)")
        
        var profileID = ""
        
        if let _profileId = advertisementData["kCBAdvDataHashedServiceUUIDs"] as? [CBUUID] {
            
            //kCBAdvDataHashedServiceUUIDs
            print("Profile ID: \(_profileId.enumerated())")
            
            let _id = _profileId[0]
            profileID = _id.representativeString()
            profileID = profileID.lowercased()
            
        }
        
        if let profileUUID = advertisementData["kCBAdvDataServiceUUIDs"] as? [CBUUID] {
            //kCBAdvDataServiceUUIDs
            print("Profile ID: \(profileUUID.enumerated())")
            
            let _id = profileUUID[0]
            profileID = _id.representativeString()
            profileID = profileID.lowercased()
        }
        
        // Even though we are conitnously receiving updates this if blcok only execute once per detection of UUID.
        if profileID != "" && discoveredProfileIds.contains(profileID) == false {
            
            self.discoveredProfileIds.append(profileID)
            print("PRINT ONE time the Discovered Profiles: \(discoveredProfileIds)")
        }
        
        print("----------------------End Central Discover----------------------\n\n\n")
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        //
        
        if central.state == .poweredOn {
            /** Scan for peripherals
             */
            // serviceUUID.append(CBUUID(string: "00000000-0000-0000-0000-000000000039")
            print("\(#function) STATE poweredON")
            centralManager.scanForPeripherals(withServices: serviceUUID, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
            print("Scanning started")
        }
        else if central.state == .poweredOff {
            print("Scanning poweredOff")
        }
        else if central.state == .unsupported {
            print("\(#line) \(#function) unsupported")
        }
        else if central.state == .unauthorized {
            
            print("\(#line) \(#function) unauthroized")
        }
        else if central.state == .resetting {
            print("\(#line) \(#function) resetting")
        }
        else {
            print("\(#line) \(#function) anything else")
        }
    }
}

extension ViewController: CBPeripheralManagerDelegate {
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        
        if (error != nil) {
            print("Error in the advertising: \(String(describing: error?.localizedDescription))")
        }
        
        print("Is peripheral advertising? : \(peripheral.isAdvertising)")
        print(peripheral)
    }
    
    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        if (peripheral.state == .poweredOn) {
            startAdvertising()
            print("\(#line) \(#function) poweredOn")
        }
        else if peripheral.state == .poweredOff {
            peripheralManager.stopAdvertising()
            print("\(#line) \(#function) poweredOff")
        }
        else if peripheral.state == .unsupported {
            peripheralManager.stopAdvertising()
            print("\(#line) \(#function) unsupported")
        }
        else if peripheral.state == .unauthorized {
            peripheralManager.stopAdvertising()
            print("\(#line) \(#function) unauthorized")
        }
        else if peripheral.state == .resetting {
           peripheralManager.stopAdvertising()
           print("\(#line) \(#function) resetting")
        }
    }
}

extension CBUUID {
    
    func representativeString() -> String {
        let data = self.data
        
        let bytesToConvert = data.count
        let uuidBytes = data //  UnsafePointer<CUnsignedChar>(data.bytes)
        var outputString = String()
        
        for currentByteIndex in 0..<bytesToConvert {
            switch currentByteIndex {
            case 3,5,7,9:
                outputString += String(format: "%02x-",uuidBytes[currentByteIndex])
            default:
                outputString += String(format: "%02x",uuidBytes[currentByteIndex])
            }
        }
        
        return outputString
    }
}

